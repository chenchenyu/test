#!/usr/bin/python
import subprocess as sb
import sys
import re
import os

for i in range(1,len(sys.argv)):
    name=sys.argv[i].split("/")
    coma='samtools view -h '+sys.argv[i]
    fo_name=name[-1]+"_thin.bam"
    fo=open(fo_name,"w")
    #print(coma)
    proc = sb.Popen(args=coma, shell=True, stdout=sb.PIPE, stderr=sb.PIPE)
    (stdout_get, stderr_get) = proc.communicate()
    arr=str(stdout_get).split("\n")
    del arr[-1]
    for k in arr:
        if(re.match('^@',k)):
            fo.write(k+"\n")
        else:
            col=k.split("\t")
            for m in range(0,11):
                fo.write(col[m]+"\t")
            if(k!=arr[-1]):
                fo.write("\n")
    commb='samtools view -b '+fo_name+" > "+name[-1]+"_trans.bam"
    commc='samtools index -b '+name[-1]+"_trans.bam"
    os.system(commb)
    os.system(commc)
    commd='rm '+fo_name
    os.system(commd)


